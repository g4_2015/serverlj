'use strict';

/**
 * @ngdoc function
 * @name serverApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the serverApp
 */
angular.module('serverApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
