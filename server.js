/* File: server.js
 * Descripcion
 * Dependecias
 * Desarrolladores
 * 2015 UNA
*/

/*Require*/
var gzippo      = require('gzippo');
var express     = require('express');
var morgan      = require('morgan');
var mongoose    = require('mongoose');
//var conf        = require('./conf/globalConfig.json');
var events;

var app = express();
var port = process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 8081;
//var ipaddr = process.env.OPENSHIFT_NODEJS_IP || conf.env.LOCALHOST_IP || '127.0.0.1';
var ipaddr = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';
var router = express.Router();
var gps;


mongoose.connect('mongodb://g4user:1234@ds059907.mongolab.com:59907/app_test');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("Conectado a mongolab");
});
var Evento      = require("./app/model/evento.js").Evento;


/***********************************************************
 ***********         ROUTER              *******************
 **********************************************************/

// middleware to use for all requests
router.use(function(req, res, next) {
    // do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});
// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/events', function(req, res) {
    res.json(events);
});

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

/***********************************************************
 ***********      SERVER                 *******************
 **********************************************************/

app.use(morgan('dev'));
app.use(gzippo.staticGzip(__dirname+"/dist"));
var server = app.listen(port, ipaddr, function () {
  console.log("Directorio "+__dirname);
  console.log("listening on "+ipaddr+":"+port);
});


/***********************************************************
 ***********      SOCKET.IO              *******************
 **********************************************************/
var io = require('socket.io').listen(server);
var clients = [];
var nsp = io.of('/elejotaserver');
nsp.on('connection', function (socket) {
  console.log('server conected '+socket.id);
  nsp.on('cordenadas', function () {
    console.log(':)');
  });
});

io.sockets.on('connection', function (socket) {
    console.log('client connected '+socket.id);
    socket.on('cordenadas', function (data) {
      var msj = {
        id: socket.id,
        lat: data.lat,
        lon: data.lon
      };
      /*if(clients.indexOf(socket) === -1) {
        nsp.emit('news', msj);
        clients.push(socket);
      }*/
    }); 

    /* data json format
    {

    }
    */

    socket.on('eventoNuevo',function (data) {
      console.log("on -> eventoNuevo");
      console.log(data);
      var coords = [];
      coords[0] = data.geo.lat;
      coords[1] = data.geo.lng;
      var evento = new Evento();
      evento.nombre       =   data.nombre;
      evento.fecha        =   data.fecha;
      evento.descripcion  =   data.descripcion;
      evento.hora         =   data.hora;
      evento.categoria    =   data.categoria;
      evento.geo          =   [ coords[0], coords[1] ];
      evento.save(function (err) {
        var ss;
        if (err) {
          console.error(err);
          ss=false;
        }else{
          ss=true;
        }
        info={
          'success':ss
      };
        socket.emit('evCreated',info);
        var prueba=Evento.find(function (err, events) {
          if (err) return console.error(err);
          socket.emit('events',info);
          socket.broadcast.emit('events',events);
      });
      });
      console.log("...listo");
    });
    socket.on('cerca',function(data){
        console.log("on -> cerca");
        enviarEventosCerca(data);

    });
    socket.on('cargarEventos',function(data) {
        console.log("on -> cargarEventos");
        Evento.find(function (err, eventsData) {
            if (err) {
                return console.error(err);
            }
            events = eventsData;
            socket.emit('events',events);
        });
    });
    socket.on('token',function(data){
      console.log(data.data);
    });
    console.log('Enviando...');
    socket.emit('text', 'Conectado con server');
    console.log('..Enviado');


    function enviarEventosCerca(data) {
        var query = Evento.find({'geo': {
          $near: [data.lat, data.lng],
          $maxDistance: 1/111.12
        }});
        query.exec(function (err, city) {
          if (err) {
            console.log(err);
            throw err;
          }
          if (!city) {
            console.log("no hay");
          }
          else {
            console.log("si hay");
            console.log(city);
            socket.emit('events',city);
          }
        });
    }
});
