var mongoose    = require('mongoose');

var eventSchema = mongoose.Schema({
  eventoID: Number,
  nombre: String,
  fecha: Date,
  hora: String,
  descripcion: String,
  categoria: String,
  geo: {type: [Number], index: '2d'}
});


var Evento = mongoose.model('Evento', eventSchema);

module.exports = {
    Evento: Evento
};
